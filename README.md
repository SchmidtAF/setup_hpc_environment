# Customize your HPC environment

The following tutorial explains how to configure your `bash` environment, when
working from a UNIX high performance computer cluster.

Topics that will be covered:

1. Configure your `.bashrc` - note, this is a hidden file (files with a '.'
   prefix will remain hidden unless you run `ls -a`).
2. Creating a custom directory for (shared) resources - specifically focussing
   on situations were your allocated home directory is too small.
3. Installing software without admin (i.e., sudo) privileges.
4. Working with 'conda' to manage python and R modules.
5. A brief outline on git and how to use it from the command line.

To efficiently write code on a HPC environment you will need a text editor,
most systems will have `vim` and `nano` installed, with the latter
recommended for first-time users, and `vim` recommended for more involved
projects. An alternative text editor to consider, also relevant for browsing
code, would be `micro`. If you have curl installed you can download and
install `micro` in your own environment by running:

```sh
curl https://getmic.ro | bash
```

You could add this, along with future programs to your own personal `bin`
directory and add this to your `PATH` environmental variable which will be
searched for possible executable programs as follows:

```sh
mkdir ~/.bin/
cd ~/.bin
curl https://getmic.ro | bash
# add to your path
./micro ~/.bashrc
export PATH="${HOME}/.bin:${PATH}"
# type <Ctrl-s><Ctrl-q> to save and close micro
# source your bashrc
source ~/.bashrc
```

Now you can simply run `micro <FILENAME>` to view and edit the content of
`<FILENAME>`.

## Set-up your `.bashrc`

A `.bashrc` is a shell script that will run every time you start an
interactive session (a shell).
That is every time you open a terminal, for example by typing Ctrl+Alt+T.

A .bashrc file is used to configure your environment, setting specific options
or adding programs/paths.
Both your cluster home directory, as well as your local machine will have a
`.bashrc`, or similar depending on the type of shell used
(e.g., mac will often come with `zsh` pre-installed).
To view your (local or HPC) `.bashrc` run `less ~/.bashrc`.

_Explanation:_
`less` is an interactive file viewer/pages, and `~` is shorthand for
your home director (try running `echo $HOME`).

_Note:_ 
While almost all clusters have use bash (the Borne-Again SHell), your local 
machine might use a different shell by default, 
for example MacOS uses zsh (i.e., z-shell). 
In those cases the local set-up may be slightly different than the set-up 
on the HPC itself. 
For example, your configuration go in `~/.zshrc` instead of `~/.bashrc`. 

### History

You can search your command line history by running `Ctrl+R` (pattern
matching search), and `Ctrl+N` and `Ctrl+P` for forward and backward
history cycling.

_NOTE_: possibly the aforementioned shortcuts do not work in your
environment. In the case please run `stty -a` in your terminal and
look for `rprnt` to find the shortcut for pattern matching search.
To cycle through your command history you will likely need to
initiate emacs terminal behaviour by adding the following to your
`.bashrc`: `set -o emacs`.

The history is initially limited to the first few hundred commands.
This can be extended by adding the following to your `.bashrc`.

```sh
# open `.bashrc`
nano ~/.bashrc
# or use nano or vim: vim ~/.bashrc

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# History settings 

# increasing history size and length
HISTSIZE=1000000
HISTFILESIZE=1000000
# alternativly, you could get unlimited history size and length by setting
# these to -1

```

Other relevant history settings to consider adding:

```sh
   
# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend
```

_Explanation:_
After closing your bashrc, type `source ~/.bashrc` to reload
the files and make sure it all works.

### Shell prompt

Looking at your command line, where you type command to bash, there will
be some minimal information on the LHS, the 'prompt'.
For example: `username@hostname:~$ `.

You can configure the prompt by specifying the `$PS1` environmental variable.
For example we might want the prompt to return the _time_, _username_, _host_,
_directory_, _gitbranch_, ending with a `$`.

_Note_: see below for `__git_ps1`.

```sh
# check if the we can use colours
# if $TERM is equal to `xterm_color or *-256colour, set color_prompt to yes
case "${TERM}" in
    xterm-color|*-256color)
        color_prompt=yes
        ;;
esac

# configure PS1 and use a default if no colours
if [ "$color_prompt" == yes ]; then
    GREEN="$(tput setaf 2)"
    BLUE="$(tput setaf 4)"
    RED="$(tput setaf 1)"
    WHITE="$(tput setaf 7)"
    RESET="$(tput setf 7)" 
    PS1='\[${GREEN}\][\A \u@\h]\[${WHITE}\]: \[${BLUE}\]\w\[${RED}\]$(__git_ps1 " (%s)")\[${WHITE}\] \$ '
else
    # u = username, A = current time, h=hostname, w=current working directing, \\=escape
    PS1="[\A \u@\h]: \w $"
fi
# clean-up
unset color_prompt force_color_prompt
```

_Explanation:_
Have a look [here](http://faculty.salina.k-state.edu/tim/unix_sg/bash/ps1.html)
for additional information on the meaning of the PS1 symbols.
`tput` might not be available in all environments, in that case used:

```sh
PS1='\[\033[01;32m\][\A \u@\h]\[\033[00m\]\]\]: \[\033[01;34m\]\]\w\e[031m\]$(__git_ps1 " (%s)")\e[m\[\033[00m\]\] \$ '
```

### Miscellaneous functions

Some of the following functions might be useful to add to your `.bashrc`

```sh

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# History 

# History calls
alias h='history 5'             # show last five lines
alias hi='history 10'           # show last ten lines
alias his='history 20'          # show last twenty lines
alias hist='history 40 | less'  # show last 40; pipe to less
alias histo='history 70 | less' # show last 70; pipe to less
# search history for pattern
function H () {
    # check if argument is supplied
    pattern=${1:?Search term is missing}
    # remove history timestamp, if needed
    htime_original=$HISTTIMEFORMAT
    HISTTIMEFORMAT=""
    # actual work: get history > invert lines > find pattern
    # history | tac | fgrep "${pattern}"
    history | fgrep "${pattern}"
    # reset history, removes `htime_original`
    trap 'export HISTTIMEFORMAT="${htime_original}"; unset htime_original' RETURN SIGINT SIGTERM
}
export -f H

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Navigation 

##### Set MARK in filetree
function mark () {
    # test if number of arguments is not 1
    if [ "$#" -ne 1 ]; then
        book='HERE'
        printf "using \`$book\` as default bookmark\n"
    else
        book=${1}
    fi
    # set mark environ
    export ${book}="$PWD"
}
export -f mark
# Usage `mark <BOOKMARKNAME>`
# cd <DIFFERENT LOCATION>
# cd $BOOKMARKNAME


# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Miscellaneous functions

##### Better mkdir
# will make the directory 
# and cd into it 
mkcdir ()
{
    mkdir -p -- "$1" &&
      cd -P -- "$1"
}

# extract the body of a file
body() {
    IFS= read -r header
    printf '%s\n' "$header"
    "$@"
}

# alias header="head -n1 | tr $'\t' '\n' | cat -n"
# print the header, adding CR after each entry
# delimiter : argument 1
# file : argument 2
function header () {
    # check if reading from standard in 
    if [ -p /dev/stdin ]; then
            delim=${1:-'\t'}
            cat | head -n1 | tr "${delim}" "\n" | cat -n
    else
        # otherwise check if there are one or two arguments 
        if [ $# == 2 ]; then 
            delim=$1
            file=$2
        # if only one used tab delim
        elif [ $# == 1 ]; then 
            delim='\t'
            file=$1
        # return helpful error message
        else
            echo 'Too many arguments, please supply the following positional arguments: '
            echo '1) an optional delmiter can be supplied as first arugment, default `\t`,' 
            echo '2) file path.'
            # exit 1
        fi 
        # the actual command
        head -n1 "${file}" | tr "${delim}" "\n" | cat -n
    fi
}
# exporting to make the function available in subshells
export -f header

```

## Accessing a cluster

As an example we will focus on UCL's myriad 
[cluster](https://www.rc.ucl.ac.uk/docs/Clusters/Myriad/).

### SSHing to the cluster

Once you have account access to Myriad, you should be able to SSH into the
machine by using the following command:

```sh
ssh <your_UCL_user_id>@myriad.rc.ucl.ac.uk
```

If you are logging in from outside the UCL firewall, you will need to go 
through UCL's gateway:

```sh
ssh <your_UCL_user_id>@ssh-gateway.ucl.ac.uk
```

after which you should be able to ssh into Myriad as above.
Each time you will be asked to supply your UCL password (the one used for 
your e-mail account). 

To make this process easier, you can edit your ssh config on your local machine
(usually found at `~/.ssh/config`) by adding the following lines:

```sh
# Gateway jump that can be used to ssh directly into myriad
Host uclgatewayjump
    Hostname ssh-gateway.ucl.ac.uk
    User <your_UCL_user_id>
    IdentityFile ~/.ssh/myriad
    
# Myriad
Host myriad
User <your_UCL_user_id>
    HostName myriad.rc.ucl.ac.uk
    proxyCommand ssh -W %h:%p uclgatewayjump
    IdentityFile ~/.ssh/myriad

# Myriad without gateway jump
# Can be used if you're already connected to the UCL network / VPN
Host myriad_nojump
    HostName myriad.rc.ucl.ac.uk
    User <your_UCL_user_id>
    IdentityFile ~/.ssh/myriad

# Individual nodes
Host myriad_l12
    Hostname login12.myriad.rc.ucl.ac.uk
    User <your_UCL_user_id>
    ProxyCommand ssh -W %h:%p uclgatewayjump
    IdentityFile ~/.ssh/myriad

Host myriad_l13
    Hostname login13.myriad.rc.ucl.ac.uk
    User <your_UCL_user_id>
    ProxyCommand ssh -W %h:%p uclgatewayjump
    IdentityFile ~/.ssh/myriad
```

_Note_: the `IdentityFile` path refers to your private ssh key.
These can be generated using the following code, which will ask you
for an optional passphrase.
Myriad will not accept keys unless they are protected through a passphrase!

Next your public get should be registered with both myriad as well as with 
the gateway. 
```sh
# ### generating an ssh-key
cd ~/.ssh # create dir if needed
ssh-keygen -f ./myriad

# ### copy the public key to the remote
ssh-copy-id -i ./myriad.pub uclgatewayjump
ssh-copy-id -i ./myriad.pub myriad
```

Here `ssh-copy-id` copies the content of `myriad.pub` to the `authorized_keys`
file on myriad, located in `~/.ssh`. If for some reason this does not
succeed, you can simply login into myriad (`ssh myriad`) using your password
and manually copy the content of `myriad.pub` key. On myriad simply type
`cat >> ~/.ssh/authorized_keys` if the file `~/.ssh/authorized_keys` already exists,
or `cat > ~/.ssh/authorized_keys` if it does not. Append the content of `myriad.pub` 
to the end of the file, closing the file using `<Ctrl>+D`.

_Note_: The above describes the general process of setting-up ssh access on 
a high performance cluster. Depending on the specific cluster there may be 
some (hopefully) slight deviations. For example, some clusters require you
to paste the publickey content to an online portal instead of appending it to
`authorized_keys`.

##### file permissions 

Please make sure the file permissions, which can be viewed by typing 
`ls -l ~/.ssh`, are as follows:

```sh
-rw------- config
-rw------- myriad
-rw-r--r-- myriad.pub
```

Here both config and your private key should only be readable and writeable by 
you the owner, and the public key should additionally be readable by 'group'
members and `others`. 

If this is not the case ssh may complain and refuse access. 
In that case try the following commands

```sh
chmod 600 ~/.ssh/config
chmod 600 ~/.ssh/myriad
chmod 644 ~/.ssh/myriad.pub
```

_Note_: please have a look here for a brief introduction on Unix 
[permissions](https://en.wikipedia.org/wiki/File-system_permissions#Notation_of_traditional_Unix_permissions)

##### passwordless access

You can optionally install `keychain` which records your ssh passphrase for a single session (until you close your
terminal, log out, etc), so you do not have to keep re-entering it.

```sh
# ### install keychain 
# check if  it is installed
keychain --version
# if not install
sudo apt install keychain # Linux
brew install keychain # MacOS

# add the following to your .bashrc 
vim ~/.bashrc
/usr/bin/keychain $HOME/.ssh/myriad 2> /dev/null
source $HOME/.keychain/$HOSTNAME-sh

# change your ~/.ssh/config by adding `AddKeysToAgent yes` 
Host myriad
    HostName myriad.rc.ucl.ac.uk
    User <USERNAME>
    AddKeysToAgent yes
    proxyCommand ssh -W %h:%p uclgatewayjump
    IdentityFile ~/.ssh/myriad

Host uclgatewayjump
    Hostname ssh-gateway.ucl.ac.uk
    User <USERNAME>
    AddKeysToAgent yes
    IdentityFile ~/.ssh/myriad
    
```

If you are using macOS, you can use the Keychain to store the passphrase for your ssh key, which will persist over
sessions, so you should never have to re-enter your passphrase.

```sh
# change your ~/.ssh/config by adding `UseKeychain yes` 

Host uclgatewayjump
    Hostname ssh-gateway.ucl.ac.uk
    User <USERNAME>
    UseKeychain yes
    AddKeysToAgent yes
    IdentityFile ~/.ssh/myriad

Host myriad
    HostName myriad.rc.ucl.ac.uk
    User <USERNAME>
    UseKeychain yes
    AddKeysToAgent yes
    proxyCommand ssh -W %h:%p uclgatewayjump
    IdentityFile ~/.ssh/myriad

```

You should now simply be able to run `ssh myriad` from your terminal to log
into the cluster.

Alternatively (and perhaps better) if `ssh-agent` is installed simply run `ssh-add ~/.ssh/<KEYNAME>`.

## Setting up git

git a great tool for version control, allowing you to track changes in your
project.
Typically `git` will be pre-installed on your computer and HPC.
[This](https://git-scm.com/book/en/v2) book is a great starting point.

Git can interface with online resources such as `gitlab` or `github`.
To repeatedly `push` commits to these online resources you will want to
generate an SSH key.
You can do this by using the command

```sh
cd ~/.ssh && ssh-keygen -f <FILENAME>
```

Now add the public key to your account, following the provided
instructions at [this link](https://docs.gitlab.com/ee/user/ssh.html).
Please add the following to `~/.ssh/config`

```sh
# GitLab.com
Host gitlab.com
    User <USERNAME>
    HostName gitlab.com
    PreferredAuthentications publickey
    IdentityFile ~/.ssh/<FILENAME>
```

It may also be useful to configure `git` by using the following commands:

```sh
git config --global user.name "your_GitLab_username"
git config --global user.email "your_GitLab_email_address"
```

This will associate any commits that you make to you as a user.

Finally, to check you `ssh` connection work, run the following lines which
should return a welcome message from GitLab:

```bash
ssh -T git@gitlab.com
```

### git settings

The following steps can improve your git workflow.
First, download the following files to your home directory:

```sh
cd ~
wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh
wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash

# make into hidden files
mv ~/git-prompt.sh ~/.git-prompt.sh
mv ~/git-completion.bash ~/.git-completion.bash
```

_Explanation:_
`.git-completion.sh` allows you to autocomplete git commands by (repeatedly)
pressing `tab`, and `.git-prompt.sh` can be used to update your shell prompt
to indicate the current branch of your project.

We will discuss the prompt soon, but first let's add these two files to
your `.bashrc`.

```sh
# open `.bashrc`
nano ~/.bashrc
# or use vim: vim ~/.bashrc

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# GIT
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# define completion for Git
# should check if present, if not wget to home
gitcomp="${HOME}/.git-completion.bash"
if [ -r "$gitcomp" ];then
    source $gitcomp
fi

# shell prompt for PS1
# should check if present, if not wget to home
ps_test="${HOME}/.git-prompt.sh"
if [ -r "$ps_test" ]; then
    source "$ps_test"
else
    # set to null otherwise
    echo ".git-prompt.sh unavailable"
    __git_ps1=
fi
```

_Explanation:_
the `[ ]` opperator should be understood as a test, which can either be `True`
or `False`, try running `man test` or have a look
[here]( https://tldp.org/LDP/abs/html/fto.html ).
Specifically, here the `-r` flag tests if you are allowed to read the file or
not.

Some further useful settings that you could consider adding to your
`.bashrc`:

```sh
# stage deleted files
alias git_adel="git status -s | grep -E '^ D' | cut -d ' ' -f3 | xargs git add --all"

# git init with standard gitignore
git_init ()
{
    # initialize a git repo
    git init
    # copy standard .gitignore
    cp $HOME/.gitignore ./.gitignore
    if [ $? -eq 0 ]; then
        printf "\n### Copied ~/.gitignore to local directory\n"
    else
        printf "\n### Failed to copy ~/.gitignore\n"
    fi
}
```

_Explanation:_
please read the manual entry for `alias`, but essentially it simply maps the
RHS to the LHS.
The git_init function initializes a git directory and copies a default
`.gitgnore` file from your `home` to the current directory.
A `.gitignore` file tells git which files should be ignored.
For example you could place the [this](./resources/example_gitignore) files
in your home directory.

### Further git settings

Similar to `bash` git also has a configuration file: `~/.gitconfig`. Here are
some of my frequently used aliases:

```sh
[core]
	editor = vim
	excludesfile = /home/amand/.gitignore
[difftool]
	prompt = true
[diff]
	tool = vimdiff
[alias]
    ; ~~~~~~~~~~~~~ log commands
    lg = log --color --graph --pretty=format:'%C(yellow)%h%Creset -%C(red)%d%Creset %s %Cgreen(%cr) %C(bold blue)[%an]%Creset' --abbrev-commit
    last = log -1 HEAD
    ; show log with added and deleted lines
    lg-changed = log --pretty=format:"%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --numstat
    ; ~~~~~~~~~~~~~ diffs
    ; show the diff of the last commit
    diff-last = diff --cached HEAD^
    ; remove changes
	unstage = reset HEAD --
    ; ~~~~~~~~~~~~~ squash
    ; squash the last n commits
    squash-n = "!f(){ git reset --soft HEAD~\"$1\";};f"
    ; squash the last n commits and concatenat commit messages
    squash-n-msg = "!f(){ git reset --soft HEAD~\"$1\" && git commit --edit -m\"$(git log --format=%B --reverse HEAD..HEAD@{1})\";};f"
    ; ~~~~~~~~~~~~~ print configurations
    config-list = config --global -l 
    alias-list = "!git config -l | grep alias"
    ; ~~~~~~~~~~~~~ list tracked files
    tracked-list = "ls-tree --full-tree --name-only -r HEAD"
[merge]
	tool = vimdiff
[color]
	ui = auto
[pretty]
    colorful = "%C(blue)%h %C(reset)%s %C(magenta)%aN %C(green ul)%ad%C(reset)"
[credential]
	helper = cache
[init]
	defaultBranch = master
```

## Home away from home

Depending on the HPC set-up, your allocated home directory size may be
tiny and insufficient to install anything on.
In these cases it is often standard practice to create a secondary
home directory in a group/project directory (which will need
to be set-up by the HPC admin).
Be sure to add this "home-away-from-home" in you `.bashrc` and
to the `PATH`.

For example this is in one of my cluster set-up's

```sh
GROUP_HOME='/hpc/dhl_ec/afschmidt'
export PATH="${GROUP_HOME}:${PATH}"
```

These two lines 1) create the environmental variable `GROUP_HOME`, and 2) add
it to the _front_ of your existing path.

Try running `echo $GROUP_HOME` and `echo $PATH`. The PATH variable is simply a
colon separated list of absolute paths, which bash will search for
executable programs you may have saved to these locations.
Essentially is spares you from having the write the entire path to the
function.

### Sharing resources

It may seem beneficial to allow other people to access the programs you might
want to install in your `$GROUP_HOME` or even create a specific directory
of programs used by multiple people.

Unless there is a single person willing to organize and maintain these programs

- ensuring multiple version of a program can co-exists, it is may be
  better to have each user install and maintain whatever software (and versions)
  they require themselves.

## Installing programs in your own home directory

Most HPC will have many programs pre-installed and universally come the
full repertoire of UNIX programs such as `sed`, `cut`, `awk` and many
more.
Often times using a combination of these programs will get the job done,
and, depending on your code, will likely be portable to most other
clusters/environments.

Nevertheless you will want to install additional software, either in your
`$HOME` directory or in your `$GROUP_HOME`.
As an example we will go over installing a (newer) version of vim, followed
by installing conda.
As a bonus we will also install `peep`, a viewer for tabular data by
*Chris Finan*.

### vim

For those unaware vim is simply nano, only better, and if you put in the time
you will never use a different text editor (possibly Emacs, if you are less
rugged).

Instead of directly updating the pre-install version of vim, we will create a
new directory mkdir ~/vim && cd !$ to clone the vim installation files to.
Next create a directory where vim will be installed mkdir ~/usr/vim.
Now, let's clone and install vim:

```bash
git clone https://github.com/vim/vim.git
# move to the src directory
cd src
# if this is an actual update clean the previous install
make distclean
# view the possible configurations ./configure --help
# here we will use
./configure --enable-python3interp=dynamic --prefix=$HOME/usr/vim --with-features=huge --with-x
# installing
make
make install

# add vim alias and path 
vim ~/.bashrc
alias vi='vim'
alias vim="${HOME}/usr/vim/bin/vim"
export PATH="${HOME}/usr/vim/bin:${PATH}"
```

_Note 1_: This already provides a first glimpse of the benefit of `git`.

_Note 2_: Have a look [here](https://gitlab.com/SchmidtAF/vim_tutorial) if you want to
learn vim for yourself.
If you do not actually want to use vim for yourself, and we know each other,
please download the following `.vimrc` file to your home directory:

```sh
cd ~
wget https://gitlab.com/SchmidtAF/vim_tutorial/-/raw/master/.minimal_vimrc
mv .minimal_vimrc .vimrc
```

This contains all my favourite vim configurations, making any hands-on help
from me (personally) more enjoyable. If you do want to work with vim yourself,
it is probably better to add chunks to your own `.vimrc` after you have tried it
for yourself and confirmed you like the workflow.

As a side note, you will find that `rc` is often used to indication a programs file configurations.

### conda

Conda is a package manager for `python` and `R` that allows the user to
specify, share and maintain software environments, allowing a multitude
of distinct versions to mutually co-exist.
Install conda as follows:

```sh
cd ~
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

Next we can install two general purpose environments, one for python, and a
second one for R.
First have a look in `./resources` and run through the two `.yml` files.
These can be used to install the `renv` and `pyenv` environment by typing:

```sh
conda env create -f ./resources/python.yml
conda env create -f ./resources/renv.yml
```

These will likely take some time to finish.
Afterwards an environment can be activated by typing
`conda activate renv` or `conda activate pyenv`.
After activating an environment you can start R or python, by simply typing
`R` or `python`.
Additional, package can be either added to the `yml` file, and used to
update the environment with.
Or, after you have activate your environment, run `conda install PACKAGE_NAME`;
see [here](https://anaconda.org/conda-forge/r-ggplot2)

For further details have a
look [here](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html).

### peep

Peep is a python wrapper of less -S, to view tabular data on the command line.
It massively improves your command line experience.

Please install as follows:

```sh 
# activate your conda environment (here we will use base)
conda activate base
# install dependencies
conda install -c cfin -c conda-forge peep
```

Peep will allow you to view a tabular file by typing `peep <FILENAME>`;
please also read `peep -h`.





